package objects;

import scala.App;
import utils.Res;
import utils.Utils;

import java.awt.*;

/**
 * Created by Federica on 12/04/17.
 */
public class Flag extends GameObject{

    private static final int WIDTH = 30;
    private static final int HEIGHT = 300;

    public Flag(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.imgObj = Utils.getImage(Res.IMG_FLAG());

    }


}
