package game;

import javafx.embed.swing.JFXPanel;
import utils.Res;
import utils.Utils;

import javax.swing.JFrame;

public class Main {

    private static final int WINDOW_WIDTH = 700;
    private static final int WINDOW_HEIGHT = 360;
    private static final String WINDOW_TITLE = "Super Mario";
    public static Platform scene;

    public static void main(String[] args) {

        JFrame finestra = new JFrame(WINDOW_TITLE);
        finestra.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        finestra.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        finestra.setLocationRelativeTo(null);
        finestra.setResizable(true);
        finestra.setAlwaysOnTop(true);

        JFXPanel jfxPanel = new JFXPanel();
        Utils.backgroundSound(Res.BACKGROUND_SOUND());

        scene = new Platform();
        finestra.setContentPane(scene);
        finestra.setVisible(true);

        Thread timer = new Thread(new Refresh());
        timer.start();
    }

}
