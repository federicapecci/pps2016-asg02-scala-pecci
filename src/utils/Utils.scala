package utils

import javax.sound.sampled.AudioInputStream
import javax.sound.sampled.AudioSystem
import javax.sound.sampled.Clip
import javax.swing._
import java.awt._
import java.net.URL
import javafx.scene.media.{Media, MediaPlayer}


/**
  * @author Federica Pecci
  */
object Utils {

  private var mediaPlayer: MediaPlayer = null

  def getResource(path: String): URL = {
    getClass.getResource(path)
  }

  def getImage(path: String): Image = {
    new ImageIcon(getResource(path)).getImage
  }

  def playSound(son: String) {
    var clip: Clip = null
    try {
      val audio: AudioInputStream = AudioSystem.getAudioInputStream(getClass.getResource(son))
      clip = AudioSystem.getClip
      clip.open(audio)
      clip.start()
    }
    catch {
      case e: Exception => e.getCause
    }
  }

  def backgroundSound(song: String): Unit ={
    val media = new Media(getResource(song).toURI.toString)
    mediaPlayer = new MediaPlayer(media)
    mediaPlayer.play()
  }

  def stopBackgroundSound(): Unit ={

    mediaPlayer.stop()
  }
}