package characters.minor;

import java.awt.Image;

import characters.minor.Antagonist;
import characters.minor.AntagonistImpl;
import utils.Res;
import utils.Utils;

public class Mushroom extends AntagonistImpl implements Antagonist {

    private static final int WIDTH = 27;
    private static final int HEIGHT = 30;
    private Image imgMushroom;

    public Mushroom(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.imgMushroom = Utils.getImage(Res.IMG_MUSHROOM_DEFAULT());

    }

    public Image getImg() {
        return imgMushroom;
    }

    public Image deadImage() {
        return Utils.getImage(this.isToRight() ? Res.IMG_MUSHROOM_DEAD_DX() : Res.IMG_MUSHROOM_DEAD_SX());
    }

}
