package characters.factory;


import characters.basic.Mario;
import characters.basic.Protagonist;
import characters.minor.Antagonist;
import characters.minor.Mushroom;
import characters.minor.Turtle;

/**
 * Created by Federica on 18/03/17.
 */
public class FactoryCharacterImpl implements FactoryCharacter {

    @Override
    public Protagonist createMario(int x, int y) {
        return new Mario(x, y) ;
    }

    @Override
    public Antagonist createMushroom(int x, int y) {
        return new Mushroom(x, y);
    }

    @Override
    public Antagonist createTurtle(int x, int y) {
        return new Turtle(x, y);
    }
}
