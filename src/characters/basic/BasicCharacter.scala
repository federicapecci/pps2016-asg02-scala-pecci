package characters.basic

import java.awt.Image
import game.Main
import objects.GameObject
import utils.Res
import utils.Utils


trait Character {
  def walk(name: String, frequency: Int): Image
}

object BasicCharacter {
  private val PROXIMITY_MARGIN: Int = 10
}

class BasicCharacter(xCoordinate: Int, yCoordinate: Int, w: Int, h: Int) extends Character {
  private var x: Int = xCoordinate
  private var y: Int = yCoordinate
  private var width: Int = w
  private var height: Int = h
  protected var moving: Boolean = false
  protected var toRight: Boolean = true
  private var counter: Int = 0
  protected var alive: Boolean = true


  def getX: Int = x

  def getY: Int = y

  protected def getWidth: Int = width

  protected def getHeight: Int = height

  def getCounter: Int = counter

  def isAlive: Boolean = alive

  def isMoving: Boolean = moving

  protected def isToRight: Boolean = toRight

  def setAlive(alive: Boolean) = this.alive = alive

  protected def setX(x: Int) = this.x = x

  protected def setY(y: Int) = this.y = y

  def setMoving(moving: Boolean) = this.moving = moving

  def setToRight(toRight: Boolean) = this.toRight = toRight

  def setCounter(counter: Int) = this.counter = counter

  def walk(name: String, frequency: Int): Image = {
    val str: String = Res.IMG_BASE + name + (if (!this.moving || {
      this.counter += 1; this.counter
    } % frequency == 0) Res.IMGP_STATUS_ACTIVE
    else Res.IMGP_STATUS_NORMAL) + (if (this.toRight) Res.IMGP_DIRECTION_DX
    else Res.IMGP_DIRECTION_SX) + Res.IMG_EXT
     Utils.getImage(str)
  }

  def move() {
    if (Main.scene.getxPos >= 0) {
      this.x = this.x - Main.scene.getMov
    }
  }

  protected def hitAhead(og: GameObject): Boolean = {
     !(this.x + this.width < og.getX || this.x + this.width > og.getX + 5 || this.y + this.height <= og.getY || this.y >= og.getY + og.getHeight)
  }

  protected def hitBack(og: GameObject): Boolean = {
     !(this.x > og.getX + og.getWidth || this.x + this.width < og.getX + og.getWidth - 5 || this.y + this.height <= og.getY || this.y >= og.getY + og.getHeight)
  }

  protected def hitBelow(og: GameObject): Boolean = {
     !(this.x + this.width < og.getX + 5 || this.x > og.getX + og.getWidth - 5 || this.y + this.height < og.getY || this.y + this.height > og.getY + 5)
  }

  protected def hitAbove(og: GameObject): Boolean = {
     !(this.x + this.width < og.getX + 5 || this.x > og.getX + og.getWidth - 5 || this.y < og.getY + og.getHeight || this.y > og.getY + og.getHeight + 5)
  }

  protected def hitAhead(pers: BasicCharacter): Boolean = {
     this.isToRight && !(this.x + this.width < pers.getX || this.x + this.width > pers.getX + 5 || this.y + this.height <= pers.getY || this.y >= pers.getY + pers.getHeight)
  }

  protected def hitBack(pers: BasicCharacter): Boolean = {
     !(this.x > pers.getX + pers.getWidth || this.x + this.width < pers.getX + pers.getWidth - 5 || this.y + this.height <= pers.getY || this.y >= pers.getY + pers.getHeight)
  }

  def hitBelow(pers: BasicCharacter): Boolean = {
     !(this.x + this.width < pers.getX || this.x > pers.getX + pers.getWidth || this.y + this.height < pers.getY || this.y + this.height > pers.getY)
  }

  def isNearby(pers: BasicCharacter): Boolean = {
     (this.x > pers.getX - BasicCharacter.PROXIMITY_MARGIN && this.x < pers.getX + pers.getWidth + BasicCharacter.PROXIMITY_MARGIN) ||
       (this.x + this.width > pers.getX - BasicCharacter.PROXIMITY_MARGIN && this.x + this.width < pers.getX + pers.getWidth + BasicCharacter.PROXIMITY_MARGIN)
  }

  def isNearby(obj: GameObject): Boolean = {
     (this.x > obj.getX - BasicCharacter.PROXIMITY_MARGIN && this.x < obj.getX + obj.getWidth + BasicCharacter.PROXIMITY_MARGIN) ||
       (this.getX + this.width > obj.getX - BasicCharacter.PROXIMITY_MARGIN && this.x + this.width < obj.getX + obj.getWidth + BasicCharacter.PROXIMITY_MARGIN)
  }


}