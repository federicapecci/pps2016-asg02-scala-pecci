package characters.basic

import game.Main
import objects.{Flag, GameObject, Piece}
import utils.Res
import utils.Utils
import java.awt._
import javax.swing.JOptionPane

/**
  * Created by Federica on 18/03/17.
  */

trait Protagonist {
  def isJumping: Boolean

  def setJumping(jumping: Boolean)

  def doJump: Image

  def contact(obj: GameObject)

  def contactPiece(piece: Piece): Boolean

  def contact(pers: BasicCharacter)
}

object ProtagonistImpl {
  private val MARIO_OFFSET_Y_INITIAL: Int = 243
  private val FLOOR_OFFSET_Y_INITIAL: Int = 293
  private val JUMPING_LIMIT: Int = 42
}



import ProtagonistImpl._
class ProtagonistImpl(x: Int, y: Int, WIDTH: Int, HEIGHT: Int) extends BasicCharacter(x, y, WIDTH, HEIGHT)
  with Protagonist {


  private val imgMario: Image = null
  private var jumping: Boolean = false
  private var jumpingExtent: Int = 0
  protected var victory: Boolean = false

  def isJumping: Boolean = jumping


  def setJumping(jumping: Boolean) = this.jumping = jumping


  def doJump: Image = {
    var str: String = null
    this.jumpingExtent += 1
    if (this.jumpingExtent < JUMPING_LIMIT) {
      if (this.getY > Main.scene.getHeightLimit) this.setY(this.getY - 4)
      else this.jumpingExtent = JUMPING_LIMIT
      str = if (this.isToRight) Res.IMG_MARIO_SUPER_DX
      else Res.IMG_MARIO_SUPER_SX
    }
    else if (this.getY + this.getHeight < Main.scene.getFloorOffsetY) {
      this.setY(this.getY + 1)
      str = if (this.isToRight) Res.IMG_MARIO_SUPER_DX
      else Res.IMG_MARIO_SUPER_SX
    }
    else {
      str = if (this.isToRight) Res.IMG_MARIO_ACTIVE_DX
      else Res.IMG_MARIO_ACTIVE_SX
      this.jumping = false
      this.jumpingExtent = 0
    }
    Utils.getImage(str)
  }

  def contact(obj: GameObject) {
    if (this.hitAhead(obj) && this.isToRight || this.hitBack(obj) && !this.isToRight) {
      Main.scene.setMov(0)
      this.setMoving(false)
    }
    if (this.hitBelow(obj) && this.jumping) {
      Main.scene.setFloorOffsetY(obj.getY)
    }
    else if (!this.hitBelow(obj)) {
      Main.scene.setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL)
      if (!this.jumping) {
        this.setY(MARIO_OFFSET_Y_INITIAL)
      }
      if (hitAbove(obj)) {
        Main.scene.setHeightLimit(obj.getY + obj.getHeight) // the new sky goes below the object
      }
      else if (!this.hitAbove(obj) && !this.jumping) {
        Main.scene.setHeightLimit(0) // initial sky
      }
    }



  }

  def contactPiece(piece: Piece): Boolean = {
    this.hitBack(piece) || this.hitAbove(piece) || this.hitAhead(piece) || this.hitBelow(piece)
  }

  def contact(pers: BasicCharacter) {
    if (this.hitAhead(pers) || this.hitBack(pers)) {
      if (pers.isAlive) {
        this.setMoving(false)
        this.setAlive(false)
      }
      else this.setAlive(true)
    }
    else if (this.hitBelow(pers)) {
      pers.setMoving(false)
      pers.setAlive(false)
    }
  }

    /*
  i call this method when mario wins
  */
    def setVictory(victory: Boolean) = this.victory = victory


    /*
  i call this method when mario wins
  */
    def isVictory: Boolean = this.victory
}