package characters.basic

import java.awt.Image
import utils.Res
import utils.Utils

object Mario {
  private val MARIO_OFFSET_Y_INITIAL: Int = 243
  private val FLOOR_OFFSET_Y_INITIAL: Int = 293
  private val WIDTH: Int = 28
  private val HEIGHT: Int = 50
  private val JUMPING_LIMIT: Int = 42
}

import Mario._
class Mario(xCoordinate: Int, yCoordinate: Int)
  extends ProtagonistImpl(xCoordinate, yCoordinate, WIDTH, HEIGHT)
  with Protagonist {

  private var imgMario: Image = Utils.getImage(Res.IMG_MARIO_DEFAULT)
  private var jumping: Boolean = false
  private var jumpingExtent: Int = 0







}